import { Component, Input, EventEmitter, Output} from '@angular/core';

@Component({
  selector: 'app-hijo',
  templateUrl: './hijo.component.html',
  styles: []
})
export class HijoComponent {

  @Input() valor: number;
  @Output() resultado: EventEmitter<object>;

  constructor() { 
    this.resultado = new EventEmitter<object>();
  }

  incrementar(valor: number) {
    this.valor += 1;
    this.resultado.emit({valor : this.valor, signo: '+', sumador : this.valor - 1});
  }
  decrementar(valor: number) {
    this.valor -= 1;
    this.resultado.emit({valor : this.valor, signo: '-', sumador : this.valor + 1});
  }
  resetear(valor: number) {
    this.valor = 0;
    this.resultado.emit({valor : this.valor, signo: 'R', sumador : 1});
  }


}
