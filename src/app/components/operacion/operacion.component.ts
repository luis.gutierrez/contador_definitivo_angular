import { Component} from '@angular/core';

@Component({
  selector: 'app-operacion',
  templateUrl: './operacion.component.html',
  styles: []
})
export class OperacionComponent  {

  resultado: number;
  sumador : number;
  signo: string;

  constructor() {
    this.resultado = 0;
    this.sumador = 1;
    this.signo = 'R';
   }

   mostrarValor(objeto: any) {
     this.resultado = objeto.valor;
     this.signo = objeto.signo;
     this.sumador = objeto.sumador;
   }
}
